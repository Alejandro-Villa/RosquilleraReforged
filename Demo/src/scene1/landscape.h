#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_assetmanager.h>

class Landscape_Nubes : public RF_Process
{
  public:
    Landscape_Nubes():RF_Process("Landscape_Nubes"){}
    virtual ~Landscape_Nubes(){}

    void Start()
    {
      graph = RF_AssetManager::Get<RF_Gfx2D>("mainmenu", "nubes_parallax");
      transform.position.y = RF_Engine::MainWindow()->height() * 0.75;
      transform.position.x =  RF_Engine::MainWindow()->width() * 0.80;
      zLayer = 0;
    }
};

class Landscape_BG : public RF_Process
{
  public:
    Landscape_BG():RF_Process("LandscapeBG"){}
    virtual ~Landscape_BG(){}

    void Start()
    {
      graph = RF_AssetManager::Get<RF_Gfx2D>("mainmenu", "mountain_parallax");
      transform.position.y = RF_Engine::MainWindow()->height() >> 1;
      position.x =  RF_Engine::MainWindow()->width() >> 1;
      zLayer = 10;
    }

    Vector2<float> position;
    void Draw()
    {
      transform.position.x = position.x + cos(RF_Engine::getWindow(window)->x()*0.001) * 100.0;
    }
};

class Landscape_Mid : public RF_Process
{
  public:
    Landscape_Mid():RF_Process("LandscapeFront"){}
    virtual ~Landscape_Mid(){}

    void Start()
    {
      graph = RF_AssetManager::Get<RF_Gfx2D>("mainmenu", "mountain_front_parallax");
      transform.position.y = RF_Engine::MainWindow()->height()*0.33;
      position.x =  RF_Engine::MainWindow()->width() * 2.0;
      zLayer = 20;
    }

    Vector2<float> position;
    void Draw()
    {
      transform.position.x = position.x + cos(RF_Engine::getWindow(window)->x()*0.001) * 500.0;
    }
};

class Title : public RF_Process
{
  public:
    Title():RF_Process("Title"){}
    virtual ~Title(){}

    void Start()
    {
      graph = RF_AssetManager::Get<RF_Gfx2D>("mainmenu", "title");
      transform.position.y = RF_Engine::MainWindow()->height()*0.20;
      position.x =  RF_Engine::MainWindow()->width() * 2.07;
      transform.scale.x = transform.scale.y = 0.35;
      zLayer = 15;
    }

    Vector2<float> position;
    void Draw()
    {
      transform.position.x = position.x + cos(RF_Engine::getWindow(window)->x()*0.001) * 300.0;
    }
};
class Avion : public RF_Process
{
  public:
    Avion():RF_Process("Avion"){}
    virtual ~Avion(){}

    void Start()
    {
      graph = RF_AssetManager::Get<RF_Gfx2D>("mainmenu", "nave");
      position.y = RF_Engine::MainWindow()->height() >> 2;
      position.x =  RF_Engine::MainWindow()->width() * 1.12;
      transform.scale.x = transform.scale.y = 0.75;
      zLayer = 16;
    }

    Vector2<float> position;
    float d = 0.0;
    float p = 0.0;
    bool dir = false;
    void Draw()
    {
      d += RF_Engine::instance->Clock.deltaTime*2;

      if(!dir)
      {
        p += RF_Engine::instance->Clock.deltaTime*150;
        transform.scale.x = 0.75;
      }
      else
      {
        p -= RF_Engine::instance->Clock.deltaTime*100;
        transform.scale.x = -0.75;
      }
      transform.position.x = (position.x + cos(RF_Engine::getWindow(window)->x()*0.001) * 300.0) + p;
      transform.position.y = position.y + cos(d) * 30.0;

      if(transform.position.x > 500.0) dir = true;
      if(transform.position.x < -50.0) dir = false;
    }
};

class Avion2 : public RF_Process
{
  public:
    Avion2():RF_Process("Avion2"){}
    virtual ~Avion2(){}

    void Start()
    {
      graph = RF_AssetManager::Get<RF_Gfx2D>("mainmenu", "malo");
      position.y = RF_Engine::MainWindow()->height() >> 3;
      position.x =  RF_Engine::MainWindow()->width() * 3.27;
      transform.scale.x = transform.scale.y = 0.75;
      zLayer = 14;
    }

    Vector2<float> position;
    float d = 0.0;
    float p = 0.0;
    bool dir = false;
    void Draw()
    {
      d += RF_Engine::instance->Clock.deltaTime*2;

      if(dir)
      {
        p += RF_Engine::instance->Clock.deltaTime*125;
        transform.scale.x = -0.75;
      }
      else
      {
        p -= RF_Engine::instance->Clock.deltaTime*125;
        transform.scale.x = 0.75;
      }

      transform.position.x = (position.x + cos(RF_Engine::getWindow(window)->x()*0.001) * 300.0) + p;
      transform.position.y = position.y + cos(d) * 30.0;

      RF_Engine::Debug(transform.position.x);
      if(transform.position.x > 500.0) dir = false;
      if(transform.position.x < -50.0) dir = true;
    }
};
