#include <RosquilleraReforged/rf_layer.h>
#include <RosquilleraReforged/rf_taskmanager.h>
#include <omp.h>

class TestProcess : public RF_Layer
{
	public:
		TestProcess():RF_Layer("TestProcess"){}
		virtual ~TestProcess(){}

		virtual void Start()
		{
			RF_Layer::Start();

			for(int i = 0; i < 6; i++)
			{
				rand();rand();rand();rand();rand();
				float aux = rand()%55;
				aux = (aux > 1.0) ? aux : 1.0;
				deriv[i] = ((float)(10 + rand()%(int)aux))/aux;

				aux = rand()%40;
				aux = (aux > 1.0) ? aux : 1.0;
				deriv2[i] = ((float)(10 + rand()%(int)aux))/aux;

				transform.position.x = transform.position.y = 125.0;
			}
		}
		virtual void Update()
		{
			if(RF_Engine::getWindow(window)==nullptr){return;}

			#pragma omp parallel for private(i)
			for(int i = 0; i < 6; i++)
			{
				position[i].x = (RF_Engine::getWindow(window)->width()>>1)+cos(3.14*p*0.1*deriv[i])*(RF_Engine::getWindow(window)->width()>>1);
				position[i].y = (RF_Engine::getWindow(window)->height()>>1)+sin(3.14*p*0.1*deriv2[i])*(RF_Engine::getWindow(window)->height()>>1);
			}

			for(int i = 0; i < 6; i++)
			{
				#pragma omp critical
				RF_Primitive::putPixel(graph, (int)position[i].x, (int)position[i].y, color[i]);
			}
			p+=RF_Engine::instance->Clock.deltaTime;
		}

		float deriv[6], deriv2[6];
		float p = 0;

		Uint32 color[6];
		Vector2<float> position[6];
};
