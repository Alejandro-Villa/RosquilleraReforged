#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>

#include "windowmover.h"
#include "testprocess.h"
#include "testprocess2.h"
#include "testprocess3.h"
#include "landscape.h"

#include <string>
using namespace std;

class Scene1 : public RF_Process
{
  public:
    Scene1():RF_Process("Scene1"){}
    ~Scene1()
    {
      RF_Engine::closeWindow(window++);
      RF_Engine::closeWindow(window++);
      RF_Engine::closeWindow(window++);
      RF_Engine::closeWindow(window++);
    }

    int w, wM;

    virtual void Start()
    {
    	RF_AssetManager::LoadAssetPackage("resources/scene1");
      	RF_AssetManager::LoadAssetPackage("resources/mainmenu");

      string proc;
      Uint32 color[6] = {0xFF0000, 0x00FF00, 0x0000FF, 0xFFFF00, 0xFF00FF, 0x00FFFF};

      w = RF_Engine::addWindow("Pintado de píxeles", 250, 250);
      window = w;
      RF_Engine::MainWindow(w);
        RF_Engine::getWindow(w)->move(Vector2<int>(RF_Engine::getWindow(w)->x(), RF_Engine::getWindow(w)->y() - 100));
        proc = RF_Engine::newTask<TestProcess>(id, w);
        for(int i = 0; i < 6; i++)
        {
          RF_Engine::getTask<TestProcess>(proc)->color[i] = color[i];
        }

    	w = RF_Engine::addWindow("Reordenamiento de capas", 250, 250, RF_Engine::MainWindow()->x() - 258,  RF_Engine::MainWindow()->y());
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("scene1", "r_b");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("scene1", "g_b");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("scene1", "b_b");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("scene1", "aux1_b");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("scene1", "aux2_b");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("scene1", "aux3_b");

    	w = RF_Engine::addWindow("Metabolas RGB", 250, 250, RF_Engine::MainWindow()->x() + 258, RF_Engine::MainWindow()->y());
    		proc = RF_Engine::newTask<TestProcess3>(id, w);

    	w = RF_Engine::addWindow("Movimiento de ventana", 350, 100, RF_Engine::MainWindow()->x() - 50,  RF_Engine::MainWindow()->y() + 315);//315);
    		proc = RF_Engine::newTask<WindowMover>(id, w);
        proc = RF_Engine::newTask<Landscape_Mid>(id, w);
        proc = RF_Engine::newTask<Title>(id, w);
        proc = RF_Engine::newTask<Avion>(id, w);
        proc = RF_Engine::newTask<Avion2>(id, w);
        proc = RF_Engine::newTask<Landscape_BG>(id, w);
        proc = RF_Engine::newTask<Landscape_Nubes>(id, w);
    }

    virtual void Update()
    {
    }
};
