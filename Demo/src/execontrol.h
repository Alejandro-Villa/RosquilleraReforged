#ifndef EXECONTROL_H
#define EXECONTROL_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_structs.h>
#include <RosquilleraReforged/rf_input.h>
#include <RosquilleraReforged/rf_engine.h>

class ExeControl : public RF_Process
{
  public:
    ExeControl():RF_Process("ExeControl"){}
    virtual ~ExeControl(){}

    virtual void Update()
    {
      if(RF_Input::key[_esc])
      {
        RF_Engine::Status() = false;
      }
    }
};

#endif //EXECONTROL_H
