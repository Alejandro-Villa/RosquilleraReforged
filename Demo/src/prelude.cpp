#include "prelude.h"

#include <RosquilleraReforged/rf_textmanager.h>
#include <RosquilleraReforged/rf_assetmanager.h>

#include "mainprocess.h"

#include <iostream>
using namespace std;

void Prelude::write_line(string text)
{
  Vector2<float> pos = Vector2<float>(10.0, 1.0);

  unsigned int siz = text.size();
  if(siz > 150)
  {
    string tmp = "";
    for(unsigned int i = 0; i < siz; i++)
    {
      if(tmp.size() >= 150)
      {
        write_line(tmp);
        tmp = "";
      }

      tmp = tmp + text[i];
    }
    write_line(tmp);
  }
  else
  {
    siz = lines.size();
    if(siz >= 35)
    {
      for(unsigned int i = 0; i < siz; i++)
      {
        pos.y = RF_Engine::getTask(lines[i])->transform.position.y;
        RF_Engine::getTask(lines[i])->transform.position.y -= 10;
      }
    }
    else if(siz > 0)
    {
      pos.y = RF_Engine::getTask(lines[siz-1])->transform.position.y;
      pos.y += 10;
    }

    lines.push_back(RF_TextManager::Write(text,{255,255,255},pos));
    if(RF_Engine::getTask(lines[lines.size()-1])->graph != nullptr)
    {
      RF_Engine::getTask(lines[lines.size()-1])->transform.position.x += RF_Engine::getTask(lines[lines.size()-1])->graph->w*0.5;
      RF_Engine::getTask(lines[lines.size()-1])->transform.position.y += RF_Engine::getTask(lines[lines.size()-1])->graph->h*0.5;
    }
  }

  if(lines.size() > 40)
  {
    for(int i = 0; i <= 5; i++)
    {
      RF_TextManager::DeleteText(lines[0]);
      lines.erase(lines.begin());
    }

    RF_Engine::Debug(lines.size());
  }
}

void Prelude::replaceLine(string text)
{
  int ultimo = lines.size()-1;
  RF_TextManager::DeleteText(lines[ultimo]);
  lines.erase(lines.begin() + ultimo);
  write_line(text);
}

void Prelude::clear()
{
  unsigned int lim = lines.size();
  for(unsigned int i = 0; i < lim; i++)
  {
    RF_TextManager::DeleteText(lines[0]);
    lines.erase(lines.begin());
  }

  RF_TextManager::DeleteText(prompt);
  prompt = "";
}

void Prelude::promptUpdate()
{
  if(prompt != "")
  {
    RF_TextManager::DeleteText(prompt);
    prompt = "";
  }
  else if(noprompt == false)
  {
    SDL_Surface* tmp = RF_Engine::getTask(lines[lines.size()-1])->graph;
    Vector2<int> tempos = Vector2<int>(10, RF_Engine::getTask(lines[lines.size()-1])->transform.position.y);
    if(tmp)
    {
      tempos.x = RF_Engine::getTask(lines[lines.size()-1])->transform.position.x + (tmp->w * 0.5);
    }

    prompt = RF_TextManager::Write("_", {255,255,255},tempos);
    RF_Engine::getTask(prompt)->transform.position.x += RF_Engine::getTask(prompt)->graph->w;
  }
}

void Prelude::Start()
{
  window = RF_Engine::addWindow("Fancy Terminal", 800, 600);
  RF_AssetManager::LoadAssetPackage("resources/prelude");
  RF_TextManager::Font = RF_AssetManager::Get<RF_Font>("prelude", "Courier_10_Pitch_BT");
  write_line("euskal@encounter:~/demoscene/open_demo_compo/yawin$ tar -xzvf PosterizedLandscape.tar.gz");

  transform.position.x = RF_Engine::getWindow(window)->width()>>1;
  transform.position.y = RF_Engine::getWindow(window)->height()>>1;
}

void Prelude::Update()
{
  delta += RF_Engine::instance->Clock.deltaTime;
  waiting += RF_Engine::instance->Clock.deltaTime;

  if(delta > 0.5)
  {
    delta = 0.0;
    promptUpdate();
  }
  if(part_count == 0)
  {
    if(waiting > 5.0)
    {
      waiting = 0.0;
      part_count++;
      term_input = new ifstream("resources/prelude/term_input.txt");
    }
  }
  else if(part_count == 1)
  {
    if(waiting >= nextdelta)
    {
      waiting = 0.0;
      nextdelta = (float)(1 + (random() % 5)) / 50.0;
      noprompt = true;

      string tmpstr;
      getline(*term_input, tmpstr);
      write_line(tmpstr);
      if(term_input->eof())
      {
        part_count++;
        term_input->close();
      }
    }
  }
  else if(part_count == 2)
  {
    noprompt = false;
    if(waiting > 3.0)
    {
      waiting = 0.0;
      part_count++;
      replaceLine("euskal@encounter:~/demoscene/open_demo_compo/yawin$ ./PosterizedLandscape version=party sound=true fun=full");
      write_line("");
      promptUpdate();
    }
  }
  else if(part_count == 3)
  {
    if(waiting > 3.0)
    {
      waiting = 0.0;
      part_count++;
      clear();
      RF_Engine::getTask<MainProcess>(father)->ChangeScene(_SPLASHSCREEN);
      RF_AssetManager::UnloadAssetPackage("prelude");
      //RF_Engine::closeWindow(window);
    }
  }
}
