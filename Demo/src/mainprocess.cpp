#include "mainprocess.h"

#include "execontrol.h"
#include "testscene.h"
#include "prelude.h"
#include "splashscreen.h"
#include "scene1/scene1.h"

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>

int startingScene = _PRELUDE;
int bpm = 60;

void MainProcess::ChangeScene(unsigned int scene)
{
  if(scene > _FOO_SCENE){return;}
  RF_Engine::sendSignal(stateMachine, S_KILL);

  switch(scene)
  {
    case _PRELUDE:
      stateMachine = RF_Engine::newTask<Prelude>(id);
      break;

    case _SPLASHSCREEN:
      stateMachine = RF_Engine::newTask<SplashScreen>(id);
      break;

    case _SCENE1:
      stateMachine = RF_Engine::newTask<Scene1>(id);
      break;

    case _FOO_SCENE:
      RF_Engine::Status() = false;
      break;

    case _TEST_SCENE:
      stateMachine = RF_Engine::newTask<TestScene>(id);
      break;
  }

  if(precharged != "")
  {
    stateMachine = precharged;
    precharged = "";
  }

  RF_Engine::Debug("[SCENE ENTERING] " + stateMachine);
}

void MainProcess::Start()
{
  RF_Engine::newTask<ExeControl>(id);
  RF_AssetManager::LoadAssetPackage("resources/common");
  ChangeScene(startingScene);
}
