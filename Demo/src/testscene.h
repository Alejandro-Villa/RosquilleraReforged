#ifndef TESTSCENE_H
#define TESTSCENE_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include "tools/beater.h"

class TestScene : public RF_Process
{
  public:
    TestScene():RF_Process("TestScene"){}
    virtual ~TestScene(){}

    virtual void Start()
    {
    	RF_AssetManager::LoadAssetPackage("resources/scene1");

      b = RF_Engine::newTask<Beater>(id);
      RF_Engine::getTask(b)->graph = RF_AssetManager::Get<RF_Gfx2D>("scene1", "r_b");
    }

    virtual void Update(){}

  private:
    string b;
};

#endif //TESTSCENE_H
