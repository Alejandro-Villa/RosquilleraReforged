#ifndef SPLASHSCREEN_H
#define SPLASHSCREEN_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_layer.h>

class SplashScreen : public RF_Layer
{
    public:
        SplashScreen():RF_Layer("SplashScreen"){}
        virtual ~SplashScreen(){}

        virtual void Start();
        virtual void Update();

    private:
        vector<SDL_Surface*> bgImg;
        float step = 0.0, stp;
        int i,j, xx, yy;
        int logo = 0, max_logos;
        bool precharged = false;
};

#endif //SPLASHSCREEN_H
