#ifndef MAINPROCESS_H
#define MAINPROCESS_H

#include <RosquilleraReforged/rf_process.h>

enum Scenes
{
  _PRELUDE = 0,
  _SPLASHSCREEN,
  _SCENE1,
  _TEST_SCENE,
  _FOO_SCENE
};

class MainProcess : public RF_Process
{
  public:
    MainProcess():RF_Process("MainProcess"){}
    virtual ~MainProcess(){}

    void ChangeScene(unsigned int scene);

    virtual void Start();

    int bpm;
    string precharged = "";

  private:
    string stateMachine = "";
};

#endif //MAINPROCESS_H
