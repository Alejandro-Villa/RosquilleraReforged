#ifndef PRELUDE_H
#define PRELUDE_H

#include <RosquilleraReforged/rf_process.h>

class Prelude : public RF_Process
{
  public:
    Prelude():RF_Process("Prelude"){}
    virtual ~Prelude(){}

    vector<string> lines;
    void write_line(string text);
    void replaceLine(string text);
    void clear();
    void promptUpdate();

    virtual void Start();

    virtual void Update();

  private:
    float delta = 0.0;
    string prompt = "";
    bool noprompt = false;

    int part_count = 0;
    float nextdelta = 0.25, waiting = 0.0f;
    ifstream *term_input;
};

#endif //PRELUDE_H
