#ifndef BEATER_H
#define BEATER_H

#include "onwindow.h"

class Beater : public OnWindow
{
  public:
    Beater(string type = "Beater"):OnWindow(type){}
    virtual ~Beater(){}

    virtual void Start();
    virtual void Update();
};

#endif //BEATER_H
