#ifndef ONWINDOW_H
#define ONWINDOW_H

#include <RosquilleraReforged/rf_process.h>
#include <RosquilleraReforged/rf_engine.h>

class OnWindow : public RF_Process
{
  public:
    OnWindow(string type = "OnWindow"):RF_Process(type){}
    virtual ~OnWindow()
    {
      RF_Engine::closeWindow(window);
    }

    string title = "OnWindow";
    virtual void Configure(int w, int h)
    {
      window = RF_Engine::addWindow(title, w, h);
    }

    template<typename T = int>
    void move(T x, T y)
    {
      if(RF_Engine::getWindow(window) != nullptr)
      {
        RF_Engine::getWindow(window)->move(Vector2<int>((int)x, (int)y));
      }
    }

    template<typename T = int>
    void moveTo(T x, T y)
    {
      if(RF_Engine::getWindow(window) != nullptr)
      {
        RF_Engine::getWindow(window)->move(Vector2<int>(RF_Engine::getWindow(window)->x() + (int)x, RF_Engine::getWindow(window)->y() + (int)y));
      }
    }

    template<typename T = int>
    void resize(T x, T y)
    {
      if(RF_Engine::getWindow(window) != nullptr)
      {
        RF_Engine::getWindow(window)->resize(Vector2<int>((int)x, (int)y));
      }
    }
};

/*class Pelota : public OnWindow
{
  public:
    Pelota():OnWindow("Pelota"){}
    virtual ~Pelota(){}

    virtual void Start()
    {
      graph = RF_AssetManager::Get<RF_Gfx2D>("scene1", "r_b");
      Configure(graph->w, graph->h);
    }

    virtual void Update()
    {
      p+= RF_Engine::instance->Clock.deltaTime;
      move((1920>>2)+cos(3.14*p*deriv)*90, (1080>>2)+sin(3.14*p*deriv2)*90);
    }

    float p;
    float deriv = 0.2, deriv2 = 0.5;
};*/

#endif //ONWINDOW_H
