#include "splashscreen.h"

#include <RosquilleraReforged/rf_primitive.h>
#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_soundmanager.h>

#include "mainprocess.h"
#include "scene1/scene1.h"

void SplashScreen::Start()
{
    //RF_Engine::instance->Clock.setFixedCTime();
    RF_SoundManager::playSong(RF_AssetManager::Get<RF_AudioClip>("common", "CaravanPalace"));

    window = RF_Engine::addWindow("Posterized Landscape", 1920, 1080, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, SDL_WINDOW_OPENGL|SDL_WINDOW_FULLSCREEN);
    RF_Engine::Debug(window);
    RF_Engine::MainWindow(window);
    setWindow(window);

    max_logos = 3;
    RF_AssetManager::LoadAssetPackage("resources/splashscreen");
    bgImg.push_back(RF_AssetManager::Get<RF_Gfx2D>("splashscreen", "banner"));
    bgImg.push_back(RF_AssetManager::Get<RF_Gfx2D>("splashscreen", "logo"));
    bgImg.push_back(RF_AssetManager::Get<RF_Gfx2D>("splashscreen", "euskal"));

    RF_Layer::Start();
    RF_Primitive::clearSurface(graph, 0xFFFFFF);

    transform.position.x = RF_Engine::MainWindow()->width() >> 1;
    transform.position.y = RF_Engine::MainWindow()->height() >> 1;

    step = 0.0;
}

void SplashScreen::Update()
{
    step += RF_Engine::instance->Clock.deltaTime*20;
    stp = (step < 50.0) ? step : ((step > 100.0) ? step - 50.0 : 50.0);

    RF_Primitive::clearSurface(graph, 0xFFFFFF);

    for(i = 0; i < bgImg[logo]->w; i+=3)
    {
      for(j = 0; j < bgImg[logo]->h; j+=3)
      {
        xx = i - ((rand()%40) - 20) * (50-stp);
        yy = j - ((rand()%40) - 20) * (50-stp);

        if(0 <= xx && 0 <= yy && bgImg[logo]->w > xx && bgImg[logo]->h > yy)
        {
          RF_Primitive::putPixel(graph, xx, yy, RF_Primitive::getPixel(bgImg[logo],i,j));
          RF_Primitive::putPixel(graph, xx, yy+1, RF_Primitive::getPixel(bgImg[logo],i,j+1));
          RF_Primitive::putPixel(graph, xx, yy+2, RF_Primitive::getPixel(bgImg[logo],i,j+2));

          RF_Primitive::putPixel(graph, xx+1, yy, RF_Primitive::getPixel(bgImg[logo],i+1,j));
          RF_Primitive::putPixel(graph, xx+1, yy+1, RF_Primitive::getPixel(bgImg[logo],i+1,j+1));
          RF_Primitive::putPixel(graph, xx+1, yy+2, RF_Primitive::getPixel(bgImg[logo],i+1,j+2));

          RF_Primitive::putPixel(graph, xx+2, yy, RF_Primitive::getPixel(bgImg[logo],i+2,j));
          RF_Primitive::putPixel(graph, xx+2, yy+1, RF_Primitive::getPixel(bgImg[logo],i+2,j+1));
          RF_Primitive::putPixel(graph, xx+2, yy+2, RF_Primitive::getPixel(bgImg[logo],i+2,j+2));
        }
      }
    }

    /*if(step > 120.0 && logo == max_logos-1 && !precharged)
    {
      RF_Engine::getTask<MainProcess>(father)->precharged = RF_Engine::newTask<Scene1>(RF_Engine::getTask<MainProcess>(father)->id);
      precharged = true;
    }
    SDL_ShowWindow( RF_Engine::getWindow(window)->Window() );*/
    if(step > 130.0)
    {
      step = 0.0;
      logo++;
      if(logo >= max_logos)
      {
        RF_Engine::closeWindow(0);
        RF_Engine::closeWindow(1);
        RF_AssetManager::UnloadAssetPackage("splashscreen");
        RF_Engine::getTask<MainProcess>(father)->ChangeScene(_SCENE1);
      }
    }
}
