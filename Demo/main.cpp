#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_debugconsole.h>
#include "mainprocess.h"

#include <iostream>
#include <string>
using namespace std;

extern int  startingScene;
extern int  bpm;

void cierra(int argc, const char *argv[])
{
	RF_Engine::Status() = false;
}

void taskList(int argc, const char *argv[])
{
	for(RF_Process *p : RF_TaskManager::instance->getTaskList())
	{
		if(p->type != "RF_Text")
		{
			RF_DebugConsoleListener::writeLine(p->id + " -> " + p->type);
		}
	}
}

int main(int argc, char *argv[])
{
	bool debug = false;

	if(argc > 1)
	{
		if(strcmp(argv[1], "--help") == 0)
		{
			cout << "Modo de uso: ./Demo [OPCIONES]" << endl;
			cout << "Ejecuta la demo" << endl << endl;
			cout << "Opciones:" << endl;
			cout << "  -d, --debug	activa el modo debug" << endl;
			cout << "  -s, --scene	inicia en la escena indicada" << endl;
			cout << "  --bpm		establece los beats por minuto" << endl << endl;
			cout << "Por ejemplo:" << endl;
			cout << "	./Demo -d --scene 1 --bpm 60" << endl << endl;
			cout << "Si tienes algún problema envía un email a <tuzmakel@gmail.com>" << endl << endl;
			exit(0);
		}

		for(int i = 1; i < argc; i++)
		{
			if(strcmp(argv[i], "--debug") == 0 || strcmp(argv[i], "-d") == 0)
			{
				debug = true;
			}
			else if(strcmp(argv[i], "--scene") == 0 || strcmp(argv[i], "-s") == 0)
			{
				startingScene = atoi(argv[++i]);
			}
			else if(strcmp(argv[i], "--bpm") == 0)
			{
				bpm = atoi(argv[++i]);
			}
		}
	}

  RF_DebugConsoleListener::addCommand("close", new RF_DebugCommand("cierra el juego", 0, &cierra));
	RF_DebugConsoleListener::addCommand("taskList", new RF_DebugCommand("lista los procesos", 0, &taskList));

	RF_Engine::Start<MainProcess>(debug);
	exit(0);
}
