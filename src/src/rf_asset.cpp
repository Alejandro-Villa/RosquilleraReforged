/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_asset.h"

RF_AssetList::RF_AssetList(string path)
{
  ///Por el momento, path es un directorio
    //Obtenemos el id del paquete de recursos
        const size_t it = path.find_last_of('/');
        id = path; id.erase(0,it+1);

    //Extraemos el nombre del directorio para insertarlo en el nombre;
        DIR *dir;

    //En *ent habrá información sobre el archivo que se está "sacando" a cada momento;
        struct dirent *ent;

    //Empezaremos a leer en el directorio actual;
        dir = opendir (path.c_str());

    //Miramos que no haya error
        if (dir == NULL)
        {
            RF_Engine::Debug(("LoadAsset [Error]: No se puede abrir directorio " + path));
            return;
        }

    //Creamos un puntero con el seleccionaremos el fichero de configuración (si existe);
        ifstream fich(path + "/package.cfg");

        if(fich.is_open())
        {
            RF_Engine::Debug((path + "/package.cfg"));

            string buffer;
            while(!fich.eof())
            {
                fich >> buffer;
                cfg.push_back(buffer);
            }
        }

    //Leyendo uno a uno todos los archivos que hay
        while ((ent = readdir (dir)) != NULL)
        {
            //Los ficheros "." y ".." son punteros al directorio actual y al directamente superior
                if((strcmp(ent->d_name, ".")!=0) && (strcmp(ent->d_name, "..")!=0))
                {
                    string ext = ent->d_name;
                    const size_t pos = ext.find_last_of('.');
                    string Aid = ext; Aid.erase(pos);
                    ext.erase(0,pos+1);

                    string p = path + "/" + ent->d_name;
                    string opc = getConfig(Aid);

                    int t = asset_type(ext);
                    if (t == 0) //Gfx2D
                    {
                        /*if(opc != "MultiSprite")
                        {*/
                            RF_Gfx2D* nA = new RF_Gfx2D(Aid, RF_Primitive::loadPNG_Surface(p.c_str()));
                            assets[Aid] = nA;
                        /*}
                        else
                        {
                            Vector2<int> size = getMultiSpriteConfig(Aid,path);
                            SDL_Surface* srf = RF_Engine::instance->loadPNG_Surface(p.c_str());
                            Vector2<int> scale(srf->w / size.x, srf->h / size.y);
                            SDL_Surface* dest = NULL;

                            for(int i = 0; i < size.x; i++)
                            {
                                for(int j = 0; j < size.y; j++)
                                {
                                    dest = SDL_CreateRGBSurface(0, scale.x, scale.y, srf->format->BitsPerPixel, 0,0,0,0);

                                    for(int ii = 0; ii < scale.x; ii++)
                                    {
                                        for(int jj = 0; jj < scale.y; jj++)
                                        {
                                            RF_Primitive::putPixel(dest, ii, jj, RF_Primitive::getPixel(srf, ii + scale.x*i, jj + scale.y*j));
                                        }
                                    }

                                    RF_Gfx2D* push = new RF_Gfx2D((Aid + "_" + to_string(i) + "_" + to_string(j)),dest);
                                    assets.push_back(push);
                                }
                            }

                            SDL_FreeSurface(srf);

                            RF_MultiSprite_Info* nI = new RF_MultiSprite_Info((Aid + "_info"),Vector2<int>(size));
                            assets.push_back(nI);
                        }*/
                    }
                    else if(t == 1) //AudioClip
                    {
                        if(opc == "-1") {opc = getConfig("AllAudio");}

                        if(opc == "fx")
                        {
                            RF_FXClip* nA = new RF_FXClip(Aid, Mix_LoadWAV(p.c_str()));
                            assets[Aid] = nA;
                        }
                        else
                        {
                            RF_AudioClip* nA = new RF_AudioClip(Aid, Mix_LoadMUS(p.c_str()));
                            assets[Aid] = nA;
                        }
                    }
                    else if(t == 2) //TTF Font
                    {
                        int pt = stoi(opc);
                        if(pt == -1){pt = 12;}
                        else if(pt < 1){pt = 1;}

                        RF_Font* nA = new RF_Font(Aid, TTF_OpenFont(p.c_str(),pt), p);
                        assets[Aid] = nA;
                    }
                    /*else if(t == 3) //Tiled Map
                    {
                        char * xml = (char*) loadFile(p.c_str(), true);
                        NLTmxMap* map = NLLoadTmxMap(xml);
                        RF_Tiled_Map* nTM = new RF_Tiled_Map(Aid, map);

                        assets.push_back(nTM);
                    }*/
                }
        }

        RF_Engine::Debug(("LoadAsset [Info]: " + id + " done"));
}

RF_AssetList::~RF_AssetList()
{
  assets.clear();
}

int RF_AssetList::asset_type(string ext)
{
    if(ext == "png" or ext == "jpg")
    {
        return 0; //Gfx2D
    }
    else if(ext == "wav" or ext == "mp3" or ext == "ogg")
    {
        return 1; //Snd
    }
    else if(ext == "ttf")
    {
        return 2; //Ttf
    }
    else if(ext == "tmx")
    {
        return 3; //Tiled
    }

    return -1;
}

string RF_AssetList::getConfig(string file)
{
    string ret = "-1";
    if(cfg.size() < 1){return ret;}

    for(int i = 0; i <= cfg.size()-1 && ret == "-1"; i++)
    {
      string f = cfg[i];
      if(f == file && i <= cfg.size()-3)
      {
        ret = cfg[i+2];
      }
    }

    return ret;
}
