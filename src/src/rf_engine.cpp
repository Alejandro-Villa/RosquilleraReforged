/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_engine.h"
#include "rf_textmanager.h"

RF_Engine* RF_Engine::instance = nullptr;
bool RF_Engine::isDebug = false;
bool RF_Engine::isRunning = false;

RF_Engine::RF_Engine(bool debug)
{
	RF_Engine::instance = this;
	isDebug = debug;

	SDL_Init(SDL_INIT_EVERYTHING);
	Debug("SDL_Init(SDL_INIT_EVERYTHING)");

	IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG);
	Debug("IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG)");

	Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096);
	Debug("Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096);");

	TTF_Init();
	Debug("TTF_Init()");

	new RF_TaskManager();
	Debug("New TaskManager");

	//Check for joysticks
	if(SDL_NumJoysticks() < 1)
	{
		Debug("Warning: No joysticks connected!" );
	}
	else
	{
		//Load joystick
		RF_Input::gGameController = SDL_JoystickOpen(0);
		if(RF_Input::gGameController == nullptr)
		{
			Debug("Warning: Unable to open game controller!");
		}
	}

	SDL_ShowCursor(0);

	isRunning = true;

	if(isDebug)
	{
		newTask<RF_DebugConsoleListener>();
	}
}

RF_Engine::~RF_Engine(){
	RF_TaskManager::instance->Clear();

	//if(NULL != music) Mix_FreeMusic(music); Habrá que parar el SoundManager
	SDL_JoystickClose(RF_Input::gGameController);
	RF_Input::gGameController = nullptr;
	Mix_Quit();
	TTF_Quit();
	IMG_Quit();
  SDL_Quit();
}

void RF_Engine::Run()
{
	assert(instance != nullptr);

  instance->wait_to_draw = 0;
  do
  {
    RF_Input::Update();
		manageSignals();
		instance->Clock.Update();
    RF_TaskManager::instance->Run();
    instance->wait_to_draw = instance->wait_to_draw + instance->Clock.deltaTime;
  }while(instance->wait_to_draw < instance->fps);

	RF_TaskManager::instance->FixedUpdate();
	RF_TaskManager::instance->Draw();
	for(auto& i : instance->windowList)
	{
		if(i.second != nullptr)
		{
			i.second->doRend();
		}
	}
}

int RF_Engine::addWindow(string i_title, int i_width, int i_height, int i_posX, int i_posY, int i_windowMode, int i_rendererMode)
{
	assert(instance != nullptr);
  RF_Window *w = new RF_Window(i_title, i_windowMode, i_posX, i_posY, i_width, i_height, i_rendererMode);
	int pos = instance->windowCount++;

  if(i_title == "")
  {
    w->title(to_string(pos));
  }

  if(instance->mainWindow == -1)
  {
    instance->mainWindow = pos;
    Debug("Asignada ventana principal [" + to_string(pos) + "]");
  }

	instance->windowList[pos] = w;

	if(isDebug && i_title != "RF Debug Console")
	{
		RF_TextManager::Font = TTF_OpenFont("./debug.ttf", 20);
		RF_TextManager::Write("Version de debug", {255,255,255}, Vector2<int>(100,15), "", instance->windowList[pos]);
		Debug("Version de debug");
	}

	return pos;
}

RF_Window* RF_Engine::getWindow(int id)
{
	assert(instance!=nullptr);
	return instance->windowList[id];
}
int RF_Engine::getWindow(RF_Window *window)
{
	assert(instance!=nullptr);
	for(auto& it : instance->windowList)
	{
		if(it.second == window)
		{
			return it.first;
		}
	}
}
RF_Window* RF_Engine::MainWindow()
{
	assert(instance!=nullptr);
	return instance->windowList[instance->mainWindow];
}
void RF_Engine::MainWindow(int id)
{
	assert(instance!=nullptr);
	if(instance->windowList[id] != nullptr)
	{
		instance->mainWindow = id;
    Debug("Asignada ventana principal [" + to_string(id) + "]");
	}
}
void RF_Engine::MainWindow(RF_Window *_window)
{
	assert(instance!=nullptr);
	instance->MainWindow(getWindow(_window));
}

void RF_Engine::closeWindow(int id)
{
	assert(instance!=nullptr);
	if(instance->windowList[id] != nullptr)
	{
		delete(instance->windowList[id]);
		instance->windowList.erase(id);

		if(instance->mainWindow == id)
		{
			instance->mainWindow = -1;
		}
	}
}
void RF_Engine::closeWindow(RF_Window *_window)
{
	assert(instance!=nullptr);
	instance->closeWindow(getWindow(_window));
}

bool RF_Engine::existsTask(string id)
{
	return RF_TaskManager::instance->existsTask(id);
}
void RF_Engine::deleteTask(string id)
{
	RF_TaskManager::instance->deleteTask(id);
}
void RF_Engine::deleteTask(RF_Process* task)
{
	RF_TaskManager::instance->deleteTask(task);
}

void RF_Engine::manageSignals()
{
	//#pragma omp parallel for
	for(string pid : RF_TaskManager::instance->getTaskIdList())
	{
		if(existsTask(pid))
		{
			RF_Process* it = getTask(pid);
			if(it != nullptr)
			{
				switch(it->signal)
				{
					case S_AWAKE_TREE:
						for(RF_Process* task : RF_TaskManager::instance->getTaskByFather(it->id))
						{
							sendSignal(task, S_AWAKE_TREE);
						}
						it->signal = S_AWAKE;
						break;

					case S_KILL:
						deleteTask(it);
						break;

					case S_KILL_TREE:
						deleteTask(it->id);
						break;

					case S_KILL_CHILD:
						for(RF_Process* task : RF_TaskManager::instance->getTaskByFather(it->id))
						{
							sendSignal(task->id, S_KILL_TREE);
						}
						it->signal = S_AWAKE;
						break;

					case S_SLEEP_TREE:
						for(RF_Process* task : RF_TaskManager::instance->getTaskByFather(it->id))
						{
							sendSignal(task, S_SLEEP_TREE);
						}
						it->signal = S_SLEEP;
						break;
				}
			}
		}
	}
}
void RF_Engine::sendSignal(string taskID, int signal)
{
	sendSignal(RF_TaskManager::instance->getTask(taskID), signal);
}
void RF_Engine::sendSignal(RF_Process* task, int signal)
{
	if(task != nullptr)
	{
		task->signal = signal;
	}
}
void RF_Engine::sendSignalByType(string type, int signal)
{
	for(RF_Process* it : RF_TaskManager::instance->getTaskByType(type))
	{
		sendSignal(it, signal);
	}
}

const int RF_Engine::Fps()
{
	assert(instance!=nullptr);
	return (int)(1.0/instance->fps);
}
void RF_Engine::Fps(int _fps)
{
	assert(instance!=nullptr);
	instance->fps = 1.0/_fps;
}
