/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.


  El objetivo del TaskManager es comunicarse con los procesos y transmitirles órdenes.
  Cuando el motor de la orden de actualizarse, transmitirá esa orden al TaskManager
*/

#include "rf_taskmanager.h"
#include "rf_engine.h"

//#include <omp.h>

RF_TaskManager* RF_TaskManager::instance = NULL;

RF_TaskManager::RF_TaskManager()
{
    if(instance == NULL)
    {
        instance = this;
    }
    else
    {
        delete this;
    }
}
RF_TaskManager::~RF_TaskManager()
{
    instance = NULL;
    //Borrar árbol
}

bool RF_TaskManager::existsTask(string id)
{
  return isInList(id);
}

RF_Process* RF_TaskManager::getTask(string id)
{
    return Get(id);
}

vector<RF_Process*> RF_TaskManager::getTaskByType(string type)
{
    pair<multimap<string, RF_Process*>::iterator,multimap<string, RF_Process*>::iterator> par = typeMap.equal_range(type);
    vector<RF_Process*> ret;
    for(auto& it = par.first; it != par.second; it++)
    {
      if(it->second != nullptr)
      {
          ret.push_back(it->second);
      }
    }
    return ret;
}

vector<RF_Process*> RF_TaskManager::getTaskByFather(string father)
{
  vector<RF_Process*> ret;
  vector<string> v;
  split(father,':',v);

  Node *it = root;
  Node *aux;
  for (int i = 1; i < v.size(); i++)
  {
      aux = it->firstChild;
      while (aux != NULL && aux->key != v[i])
      {
          aux = aux->son;
      }

      if (aux != NULL)
      {
        if(aux->data != nullptr && aux->data->id == father)
        {
          Node *son = aux->firstChild;
          while(son != nullptr)
          {
            ret.push_back(son->data);
            son = son->son;
          }
          return ret;
        }
        else
        {
          it = aux;
        }
      }
  }

  return ret;
}

vector<RF_Process*> RF_TaskManager::getTaskList()
{
  vector<RF_Process*> ret;
  for(auto& it : taskMap)
  {
    if(it.second != nullptr)
    {
      ret.push_back(it.second);
    }
  }

  return ret;
}

vector<string> RF_TaskManager::getTaskIdList()
{
  vector<string> ret;
  for(auto& it : taskMap)
  {
    if(it.second != nullptr)
    {
      ret.push_back(it.first);
    }
  }

  return ret;
}

void RF_TaskManager::deleteTask(string id)
{
  RemoveAll(id);
}
void RF_TaskManager::deleteTask(RF_Process* task)
{
  RemoveAll(task->id);
}

void RF_TaskManager::Run()
{
  for(auto& it : taskMap)
  {
    if(it.second != nullptr && it.second->signal == S_AWAKE)
    {
      it.second->Update();
    }
  }
}

void RF_TaskManager::FixedUpdate()
{
  /*#pragma omp parallel
  {*/
    for(auto& it : taskMap)
    {
      if(it.second != nullptr && it.second->signal == S_AWAKE)
      {
        //#pragma omp task
        it.second->FixedUpdate();
      }
    }
  //}
}

void RF_TaskManager::Draw()
{
  for(auto e = taskMap.begin(); e != taskMap.end(); e++)
  {
    if(e->second != nullptr)
    {
      e->second->Draw();
      if(e->second->graph != NULL && e->second->window != -1 && RF_Engine::getWindow(e->second->window) != nullptr)
      {
          vector<RF_Process*>::iterator it = toRend.begin(), first = toRend.begin(), last = toRend.end();
          iterator_traits<vector<RF_Process*>::iterator>::difference_type count, step;
          count = std::distance(first, last);

          while(count > 0)
          {
            it = first; step=count/2; std::advance (it,step);
            if (!(e->second->zLayer<(*it)->zLayer))
            {
              first=++it; count-=step+1;
            }
            else
            {
              count=step;
            }
          }
          if(e->second->signal != S_SLEEP && e->second->signal != S_SLEEP_TREE)
          {
            toRend.insert(it, e->second);
          }
      }
    }
  }

  for(auto it : toRend)
  {
    it->LateDraw();
    RF_Engine::getWindow(it->window)->Rend(it->graph, &it->transform, it->flipType);
    it->AfterDraw();
  }

  toRend.clear();
}

void RF_TaskManager::Clear()
{
  RemoveAll();
}
