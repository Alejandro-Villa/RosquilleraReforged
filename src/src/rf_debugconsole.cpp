/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_debugconsole.h"
#include "rf_engine.h"
#include "rf_textmanager.h"
#include <unistd.h>
#include <pthread.h>
#include <signal.h>

RF_DebugConsole* RF_DebugConsole::instance = nullptr;

RF_DebugConsole::~RF_DebugConsole()
{
  if(instance->id == id)
  {
    instance = nullptr;
  }

  RF_Engine::closeWindow(window);
  lines.clear();
}

void RF_DebugConsole::Start()
{
  if(instance != nullptr)
  {
    signal = S_KILL;
    return;
  }

  instance = this;
  window = RF_Engine::addWindow("RF Debug Console", 640, 480, 0, 0, SDL_WINDOW_INPUT_FOCUS);
}

void RF_DebugConsole::writeLine(string text)
{
  changeOffset(-(text.size()));

  TTF_Font* Font = RF_TextManager::Font;
  RF_TextManager::Font = TTF_OpenFont("./debug.ttf", 10);

  unsigned int siz = text.size();
  string txt = "";
  for(unsigned int i = 0; i < siz; i++)
  {
    txt = txt+text[i];
    if(i%dims.x == dims.x-1 && i != 0)
    {
      lines.push_back(RF_TextManager::Write(txt,{255,255,255},{5,(offSet+(int)lines.size()*10)}, id, RF_Engine::getWindow(window)));
      RF_Engine::getTask<RF_Process>(lines[lines.size()-1])->transform.position.x += RF_Engine::getTask<RF_Process>(lines[lines.size()-1])->graph->w*0.5;
      changeOffset(-1);
      txt = "";
    }
  }
  if(txt != "")
  {
    lines.push_back(RF_TextManager::Write(txt,{255,255,255},{5,(offSet+(int)lines.size()*10)}, id, RF_Engine::getWindow(window)));
    RF_Engine::getTask<RF_Process>(lines[lines.size()-1])->transform.position.x += RF_Engine::getTask<RF_Process>(lines[lines.size()-1])->graph->w*0.5;
    changeOffset(-1);
  }

  RF_TextManager::Font = Font;
}

void RF_DebugConsole::changeOffset(int modifier)
{
  int size = (int)lines.size();
  if(offSet+modifier > 0){offSet = 0;}
  else if(offSet+modifier < dims.y-size)
  {
    if(size > dims.y){offSet = dims.y-size;}
    else{offSet = 0;}
  }
  else{offSet+=modifier;}

  string pid = "";
  for(int i = 0; i < lines.size(); i++)
  {
    pid = lines[i];
    RF_Engine::getTask<RF_Process>(pid)->transform.position.y=((offSet + i)*10) + RF_Engine::getTask<RF_Process>(pid)->graph->h*0.5;
  }
}


RF_DebugConsoleListener* RF_DebugConsoleListener::instance = nullptr;
map<string, RF_DebugCommand*> RF_DebugConsoleListener::commands;

RF_DebugConsoleListener::~RF_DebugConsoleListener()
{
  if(instance->id == id)
  {
    instance = nullptr;
  }
}

void RF_DebugConsoleListener::Start()
{
  if(instance != nullptr)
  {
    signal = S_KILL;
    return;
  }

  instance = this;
  //RF_Engine::newTask<RF_SocketDebugger>(id);
  new RF_SocketDebugger();

  for(int i=0;i<_FOO_KEY;i++)
  {
      keyPressed[i]=false;
  }

  addCommand("help", new RF_DebugCommand("muestra esta ayuda", 0, &RF_DebugConsoleHelpCommand));

  writeLine("$ > ");
}

void RF_DebugConsoleListener::Update()
{
  if(RF_Input::key[_return] && RF_Input::key[_tab])
  {
    if(!keyPressed[_tab] && !keyPressed[_return])
    {
      hideConsole = !hideConsole;
      onKeyPress();
      keyPressed[_return] = true;
      keyPressed[_tab] = true;
    }
  }
  else
  {
    if(!RF_Input::key[_return])
    {
      keyPressed[_return] = false;
    }
    if(!RF_Input::key[_tab])
    {
      keyPressed[_tab] = false;
    }
  }

  if(RF_DebugConsole::instance == nullptr || !RF_Engine::getWindow(RF_DebugConsole::instance->window)->hasFocus()){return;}
  if(!hideConsole)
  {
    for(int i=0;i<=_0;i++)
    {
      if(RF_Input::key[i])
      {
        if(!keyPressed[i])
        {
          addLetter((!RF_Input::key[_left_shift] && !RF_Input::key[_right_shift]) ? letters[i].lower : letters[i].upper);
          keyPressed[i] = true;
        }
      }
      else
      {
        keyPressed[i] = false;
      }
    }
    for(int i=_minus;i<=_slash;i++)
    {
      if(RF_Input::key[i])
      {
        if(!keyPressed[i])
        {
          addLetter((!RF_Input::key[_left_shift] && !RF_Input::key[_right_shift]) ? letters[i-5].lower : letters[i-5].upper);
          keyPressed[i] = true;
        }
      }
      else
      {
        keyPressed[i] = false;
      }
    }

    if(RF_Input::key[_space]){if(!keyPressed[_space]){addLetter(" "); keyPressed[_space] = true;}}
    else{keyPressed[_space] = false;}


    if(RF_Input::key[_backspace])
    {
      if(backspaceDelay <= 0.0)
      {
        backspaceDelay = 0.1;
        removeLetter();
      }
      else
      {
        backspaceDelay -= RF_Engine::instance->Clock.deltaTime;
      }
    }
    else
    {
      backspaceDelay = 0.0;
    }

    if(RF_Input::key[_return]){if(!keyPressed[_return]){checkCommand(); keyPressed[_return] = true;}}
    else{keyPressed[_return] = false;}
  }
}

void RF_DebugConsoleListener::checkCommand()
{
  keyCount = 0;
  vector<string> c = splitLine(lines[lines.size()-1], " ");

  if(c[2] == "")
  {
    writeLine("Debes introducir un comando");
    writeLine("$ > ");
    return;
  }

  if(commands[c[2]] == nullptr)
  {
    writeLine("No se ha encontrado el comando \"" + c[2] + "\"");
    writeLine("$ > ");
    return;
  }

  int argc = c.size()-3;
  const char *argv[argc];
  for(int i = 0; i < argc; i++)
  {
    argv[i] = c[i+3].c_str();
  }

  RF_DebugCommand* com = commands[c[2]];
  if(argc < com->nargs){writeLine("Faltan argumentos");}
  else if(argc > com->nargs){writeLine("Sobran argumentos");}
  else{com->callback(argc, argv);}

  writeLine("$ > ");
  return;
}

void RF_DebugConsoleListener::addLetter(string letter)
{
  if(keyCount+1 > 100 || letter == ""){return;}
  keyCount++;
  alterLine(lines[lines.size()-1]+letter);
}

void RF_DebugConsoleListener::removeLetter()
{
  if(keyCount-1 < 0){return;}
  keyCount--;
  lines[lines.size()-1].pop_back();
  alterLine(lines[lines.size()-1]);
}

RF_Letter RF_DebugConsoleListener::letters[] = {
  RF_Letter("a","A"), RF_Letter("b","B"), RF_Letter("c","C"), RF_Letter("d","D"),
  RF_Letter("e","E"), RF_Letter("f","F"), RF_Letter("g","G"), RF_Letter("h","H"),
  RF_Letter("i","I"), RF_Letter("j","J"), RF_Letter("k","K"), RF_Letter("l","L"),
  RF_Letter("m","M"), RF_Letter("n","N"), RF_Letter("o","O"), RF_Letter("p","P"),
  RF_Letter("q","Q"), RF_Letter("r","R"), RF_Letter("s","S"), RF_Letter("t","T"),
  RF_Letter("u","U"), RF_Letter("v","V"), RF_Letter("w","W"), RF_Letter("x","X"),
  RF_Letter("y","Y"), RF_Letter("z","Z"), RF_Letter("1","!"), RF_Letter("2","\""),
  RF_Letter("3",""), RF_Letter("4","$"), RF_Letter("5","%"), RF_Letter("6","&"),
  RF_Letter("7","/"), RF_Letter("8","("), RF_Letter("9",")"), RF_Letter("0","="),
  RF_Letter("'","?"), RF_Letter("",""), RF_Letter("[","["), RF_Letter("+","]"),
  RF_Letter("}","}"), RF_Letter("",""), RF_Letter("",""), RF_Letter("{","{"),
  RF_Letter("º","\\"), RF_Letter(",",";"), RF_Letter(".",":"), RF_Letter("-","_")
};

void RF_DebugConsoleListener::onKeyPress()
{
  if(hideConsole)
  {
    RF_Engine::sendSignal(RF_DebugConsole::instance, S_KILL);
  }
  else
  {
    RF_Engine::newTask<RF_DebugConsole>(id);
    for(string txt : lines)
    {
      RF_DebugConsole::instance->writeLine(txt);
    }
  }
}

void RF_DebugConsoleListener::writeLine(string text)
{
  if(RF_DebugConsoleListener::instance == nullptr){return;}
  RF_DebugConsoleListener::instance->lines.push_back(text);
  if(RF_DebugConsole::instance != nullptr)
  {
    RF_DebugConsole::instance->writeLine(text);
  }
}

void RF_DebugConsoleListener::alterLine(string text)
{
  lines[lines.size()-1] = text;
  if(RF_DebugConsole::instance != nullptr)
  {
    int last = RF_DebugConsole::instance->lines.size()-1;
    RF_TextManager::DeleteText(RF_DebugConsole::instance->lines[last]);
    RF_DebugConsole::instance->lines.erase(RF_DebugConsole::instance->lines.begin() + last);
    RF_DebugConsole::instance->writeLine(text);
  }
}

int RF_DebugConsoleListener::linesSize()
{
  return lines.size();
}

string RF_DebugConsoleListener::getLine(int i)
{
  return lines[i];
}

void RF_DebugConsoleHelpCommand(int argc, const char* argv[])
{
  RF_DebugConsoleListener::writeLine("Lista de comandos");
  for(auto it = RF_DebugConsoleListener::instance->commands.begin(); it != RF_DebugConsoleListener::instance->commands.end(); it++)
  {
    if(it->second != nullptr)
      RF_DebugConsoleListener::writeLine("  " + it->first + " - " + it->second->description);
  }
}

RF_SocketDebugger::RF_SocketDebugger()
{
  slen = sizeof(serv_addr);

  //create a UDP socket
  if((sock = socket(AF_INET, SOCK_DGRAM, 0)) == -1)
  {
    RF_Engine::Debug("[Error] No se ha levantado DebugSocket");
    return;
  }
  else
  {
    RF_Engine::Debug("DebugSocket levantado en el puerto " + to_string(port));
  }

  bzero((char *) &serv_addr, sizeof(serv_addr));
  bzero((char *) &sock_in, sizeof(sock_in));
  sock_in_len = sizeof(sock_in);

  serv_addr.sin_addr.s_addr = inet_addr(ip.c_str());
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons(port);

  //bind socket to port
  if(bind(sock, (struct sockaddr *)&serv_addr, slen) == -1)
  {
    RF_Engine::Debug("[ERROR] No se ha podido bindear DebugSocket (" + (string)strerror(errno) + ")");
    return;
  }
  else
  {
    RF_Engine::Debug("DebugSocket bindeado");
  }

  instance = this;
  pthread_create(&listener, NULL, socketListen, NULL);
}

RF_SocketDebugger* RF_SocketDebugger::instance = nullptr;
void* socketListen(void *v)
{
  RF_SocketDebugger::instance->Listen();
}
void RF_SocketDebugger::Listen()
{
  do
  {
    recv_len = 0;
    recv_len = recvfrom(sock, buffer, DEBUG_BUFLEN, 0, (struct sockaddr*) &sock_in, &sock_in_len);
    if (recv_len != -1)
    {
      sendto(sock, buffer, recv_len, 0, (struct sockaddr*) &sock_in, sizeof(sock_in));

      string s;
      for(int i = 0; i < recv_len; i++)
      {
        s = buffer[i];
        RF_DebugConsoleListener::instance->addLetter(s);
      }
      RF_DebugConsoleListener::instance->checkCommand();

      string n = to_string(RF_DebugConsoleListener::instance->linesSize()-r);
      sendto(sock, n.c_str(), n.size(), 0, (struct sockaddr*) &sock_in, sizeof(sock_in));
      for(; r < RF_DebugConsoleListener::instance->linesSize(); r++)
      {
        sendto(sock, RF_DebugConsoleListener::instance->getLine(r).c_str(), RF_DebugConsoleListener::instance->getLine(r).size(), 0, (struct sockaddr*) &sock_in, sizeof(sock_in));
      }
    }
  } while(instance->signal == S_AWAKE && RF_Engine::Status());
  pthread_exit(NULL);
}

string RF_SocketDebugger::ip = "127.0.0.1";
int RF_SocketDebugger::port = 50002;
