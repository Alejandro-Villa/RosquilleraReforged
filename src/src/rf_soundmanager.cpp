/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_soundmanager.h"
#include "rf_assetmanager.h"
#include "rf_engine.h"

Mix_Music* RF_SoundManager::music;
Mix_Music* RF_SoundManager::cMusic;

void RF_SoundManager::playFX(string package, string file, int channel)
{
        playFX(RF_AssetManager::Get<RF_FXClip>(package, file), channel);
}

void RF_SoundManager::playFX(Mix_Chunk* clip, int channel)
{
    Mix_PlayChannel(channel, clip, 0);
}

void RF_SoundManager::playSong(string package, string file, int loop)
{
        playSong(RF_AssetManager::Get<RF_AudioClip>(package, file),loop);
}

void RF_SoundManager::playSong(Mix_Music* clip, int loop)
{
        music = clip;
        Mix_PlayMusic(music, loop);
}

void RF_SoundManager::changeMusic(string package, string file)
{
    changeMusic(RF_AssetManager::Get<RF_AudioClip>(package, file));
}

void RF_SoundManager::changeMusic(Mix_Music* clip)
{
    if(nullptr == music)
    {
        playSong(clip,-1);
    }
    else
    {
        if(cMusic == NULL)
        {
            cMusic = clip;

            if(cMusic == music)
            {
                //RF_Engine::Debug("SoundManager [Info]: Ya está sonando el audio indicado por lo que se descarta la transición");
                cMusic = NULL;
            }
            else
            {
                RF_Engine::newTask<RF_SoundTransition>();
            }
        }
        else
        {
            //RF_Engine::Debug("SoundManager [Error]: Ya hay una transición en marcha");
        }
    }
}

void RF_SoundTransition::Start()
{
  Mix_FadeOutMusic(500);
}

void RF_SoundTransition::Update()
{
  if(!changed)
  {
    if(Mix_FadingMusic() == MIX_NO_FADING)
    {
      RF_SoundManager::music = RF_SoundManager::cMusic;
      RF_SoundManager::cMusic = NULL;
      Mix_FadeInMusic(RF_SoundManager::music, -1, 500);

      changed = true;
    }
  }
  else
  {
    if(Mix_FadingMusic() == MIX_NO_FADING)
    {
      RF_Engine::sendSignal(id, S_KILL);
    }
  }
}
