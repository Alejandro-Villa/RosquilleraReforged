/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_window.h"
#include "rf_primitive.h"
#include "rf_textmanager.h"
#include <iostream>
using namespace std;

RF_Window::RF_Window(string i_title, int i_windowMode, int i_posX, int i_posY, int i_width, int i_height, int i_rendererMode)
{
    _title=i_title;
    _windowMode=i_windowMode | SDL_WINDOW_OPENGL | SDL_WINDOW_MOUSE_FOCUS;
    transform.position.x=i_posX;
    transform.position.y=i_posY;
    transform.scale.x=i_width;
    transform.scale.y=i_height;
    _rendererMode=i_rendererMode;
    _index=0;

    window = SDL_CreateWindow(_title.c_str(), transform.position.x, transform.position.y, transform.scale.x, transform.scale.y, _windowMode);
    renderer = SDL_CreateRenderer(window,_index,_rendererMode);

    SDL_GetWindowPosition(window, &transform.position.x, &transform.position.y);
    SDL_GetWindowSize(window, &transform.scale.x, &transform.scale.y);
}

RF_Window::~RF_Window()
{
  Dispose();
}

void RF_Window::Dispose()
{
  RF_TextManager::CleanWindow(RF_Engine::getWindow(this));
  SDL_DestroyWindow(window);
  SDL_DestroyRenderer(renderer);
}

void RF_Window::Rend(SDL_Surface *srf, Transform2D<float, float, float> *transform, SDL_RendererFlip flipType)
{
  _Rend(srf, Vector2<int>((int)transform->position.x, (int)transform->position.y), transform->scale, transform->rotation, flipType);
}
void RF_Window::Rend(SDL_Surface *srf, Transform2D<int, float, float> *transform, SDL_RendererFlip flipType)
{
  _Rend(srf, transform->position, transform->scale, transform->rotation, flipType);
}

void RF_Window::_Rend(SDL_Surface *srf, Vector2<int> position, Vector2<float> scale, float rotation, SDL_RendererFlip flipType)
{
    if(!prepared)
    {
        SDL_RenderClear(renderer);
        prepared = true;
    }

    SDL_Rect r;
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, srf);

    SDL_QueryTexture(texture,NULL,NULL,&r.w,&r.h);
    r.w = srf->w * scale.x;
    r.h = srf->h * scale.y;
    r.x = position.x - r.w*0.5;
    r.y = position.y - r.h*0.5;

    //SDL_RenderCopy(renderer,texture,NULL,&r);
    SDL_RenderCopyEx(renderer, texture, NULL, &r, (float)rotation, NULL, flipType);
    SDL_DestroyTexture(texture);
}
void RF_Window::doRend()
{
    if(!prepared)
    {
        SDL_RenderClear(renderer);
        prepared = true;
    }

    SDL_RenderPresent(renderer);
    prepared = false;
}

void RF_Window::setBackColor(Uint8 r, Uint8 g, Uint8 b)
{
  // Select the color for drawing. It is set to red here.
    SDL_SetRenderDrawColor(renderer, r, g, b, 255);

  // Clear the entire screen to our selected color.
    SDL_RenderClear(renderer);
}

void RF_Window::move(Vector2<int> pos)
{
    transform.position.x = pos.x;
    transform.position.y = pos.y;

    SDL_SetWindowPosition(window, transform.position.x, transform.position.y);
}

bool RF_Window::hasFocus()
{
  Uint32 flags = SDL_GetWindowFlags(window);
  return (SDL_WINDOW_INPUT_FOCUS == (flags & SDL_WINDOW_INPUT_FOCUS));
}

void RF_Window::resize(Vector2<int> scal)
{
    transform.scale.x = scal.x;
    transform.scale.y = scal.y;

    SDL_SetWindowSize(window, transform.scale.x, transform.scale.y);
}
