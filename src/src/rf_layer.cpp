/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_layer.h"
#include "rf_engine.h"
#include <limits>
#include <assert.h>
using namespace std;

RF_Layer::~RF_Layer()
{
  SDL_FreeSurface(graph);
}

void RF_Layer::Start()
{
  zLayer = -1;
  if(window != -1)
  {
    graph = SDL_CreateRGBSurface(0, RF_Engine::getWindow(window)->width(), RF_Engine::getWindow(window)->height(), 32, 0, 0, 0, 0);
  }
}

void RF_Layer::setWindow(int _window)
{
  assert(_window != -1);
  window = _window;

  if(graph != nullptr)
  {
    SDL_FreeSurface(graph);
  }
  graph = SDL_CreateRGBSurface(0, RF_Engine::getWindow(window)->width(), RF_Engine::getWindow(window)->height(), 32, 0, 0, 0, 0);
}
