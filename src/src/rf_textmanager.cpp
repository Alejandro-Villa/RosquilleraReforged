/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "rf_textmanager.h"
#include "rf_taskmanager.h"
#include <assert.h>

TTF_Font* RF_TextManager::Font = nullptr;

void RF_TextManager::DeleteText(string id)
{
  if(id == "")
  {
    vector<RF_Process*> textList = RF_TaskManager::instance->getTaskByType("Text");
    for(RF_Process* e : textList)
    {
      RF_TaskManager::instance->deleteTask(e);
    }
  }
  else
  {
    RF_TaskManager::instance->deleteTask(id);
  }
}


void RF_TextManager::CleanWindow(int window)
{
  for(auto& it : RF_TaskManager::instance->getTaskByType("RF_Text"))
  {
    if(it->window == window)
    {
      it->signal = S_KILL;
    }
  }
}

RF_Text::~RF_Text()
{
  SDL_FreeSurface(graph);
}

void RF_Text::Configure(string text, SDL_Color color, Vector2<int> position)
{
  graph = TTF_RenderUTF8_Blended(RF_TextManager::Font, text.c_str(), color);
  transform.position.x = (float)position.x;
  transform.position.y = (float)position.y;
  zLayer = numeric_limits<int>::max();
}
