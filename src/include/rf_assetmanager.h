/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_ASSETMANAGER_H
#define RF_ASSETMANAGER_H

#include "rf_asset.h"
#include "rf_engine.h"
#include "rf_primitive.h"

#include <type_traits>
#include <memory>
#include <string>
using namespace std;

class RF_AssetManager
{
  public:
    static void LoadAssetPackage(string package);
    static void UnloadAssetPackage(string package);
    static int PackageSize(string package);
    static int Size();

    static bool isLoaded(string package, string id = "");

    template <typename T>
    static typename T::element_type Get(string package, string id)
    {
      static_assert(std::is_base_of<RF_Asset, T>::value, "T must derive from RF_Asset");
      assert(isLoaded(package, id) == true);

      return assetManager[package]->Get<T>(id);
    }

  private:
    static unordered_map<string, RF_AssetList*> assetManager;
};
#endif //RF_ASSETMANAGER_H
