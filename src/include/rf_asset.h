/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_ASSET_H
#define RF_ASSET_H

#include "rf_engine.h"
#include "rf_primitive.h"

#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

#include <type_traits>
#include <memory>
#include <dirent.h>
#include <fstream>
#include <unordered_map>
#include <string>
using namespace std;

class RF_Asset
{
  public:
    RF_Asset(string name){id = name;}
    virtual ~RF_Asset(){}

    string id;

    template <typename T>
    typename T::element_type Get()
    {
      static_assert(std::is_base_of<RF_Asset, T>::value, "T must derive from RF_Asset");
      return reinterpret_cast<typename T::element_type>(GetSource());
    }

  private:
    virtual void* GetSource() = 0;
};

class RF_AudioClip : public RF_Asset
{
  public:
    RF_AudioClip(string name, Mix_Music* song):RF_Asset(name)
    {
      clip = song;
    }

    virtual ~RF_AudioClip()
    {
      Mix_FreeMusic(clip);
      clip = nullptr;
    }

    Mix_Music* clip;

    void* GetSource() { return clip; }
    typedef Mix_Music* element_type;
};

class RF_FXClip : public RF_Asset
{
  public:
    RF_FXClip(string name, Mix_Chunk* fx):RF_Asset(name)
    {
      clip = fx;
    }
    virtual ~RF_FXClip()
    {
      Mix_FreeChunk(clip);
      clip = nullptr;
    }

    Mix_Chunk* clip;

    void* GetSource() { return clip; }
    typedef Mix_Chunk* element_type;
};

class RF_Gfx2D : public RF_Asset
{
  public:
    RF_Gfx2D(string name, SDL_Surface* gfx):RF_Asset(name)
    {
      surface = gfx;
    }

    virtual ~RF_Gfx2D()
    {
      SDL_FreeSurface(surface);
      surface = nullptr;
    }

    SDL_Surface* surface;

    void* GetSource() { return surface; }
    typedef SDL_Surface* element_type;
};

class RF_Font : public RF_Asset
{
  public:
    RF_Font(string name, TTF_Font* ttf, string _path):RF_Asset(name)
    {
      path = _path;
      font = ttf;
    }

    virtual ~RF_Font()
    {
      TTF_CloseFont(font);
      font = nullptr;
    }

    TTF_Font* font;
    string path;

    void* GetSource() { return font; }
    typedef TTF_Font* element_type;
};

class RF_AssetList
{
  public:
    RF_AssetList(string path);
    virtual ~RF_AssetList();

    string id;
    unordered_map<string, RF_Asset*> assets;

    template <typename T>
    typename T::element_type Get(string id)
    {
      static_assert(std::is_base_of<RF_Asset, T>::value, "T must derive from RF_Asset");
      return assets[id]->Get<T>();
    }

  private:
    int asset_type(string ext);
    string getConfig(string file);
    vector<string> cfg;
};

#endif //RF_ASSET_H
