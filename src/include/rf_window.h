/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_WINDOW_H
#define RF_WINDOW_H

class RF_Process;
#include "rf_structs.h"
#include <SDL2/SDL.h>
#include <string>
#include <vector>
using namespace std;

class RF_Window
{
    public:
        RF_Window(string i_title, int i_windowMode, int i_posX, int i_posY, int i_width, int i_height, int i_rendererMode);
        void Dispose();

        void Rend(SDL_Surface *srf, Transform2D<int, float, float> *position, SDL_RendererFlip flipType = SDL_FLIP_NONE);
        void Rend(SDL_Surface *srf, Transform2D<float, float, float> *position, SDL_RendererFlip flipType = SDL_FLIP_NONE);
        void _Rend(SDL_Surface *srf, Vector2<int> position, Vector2<float> scale, float rotation, SDL_RendererFlip flipType = SDL_FLIP_NONE);

        void doRend();
        virtual ~RF_Window();

        const int& width() const {return transform.scale.x;}
        const int& height() const {return transform.scale.y;}

        const int& x() const {return transform.position.x;}
        const int& y() const {return transform.position.y;}

        SDL_Window* Window() {return window;}

        const string& title() const {return _title;}
        void title(string newTitle)
        {
            _title = newTitle;
            SDL_SetWindowTitle(window, _title.c_str());
        }

        void setBackColor(Uint8 r, Uint8 g, Uint8 b);

        void move(Vector2<int> pos);
        void resize(Vector2<int> scal);

        bool hasFocus();

        SDL_Renderer*   renderer;

    private:
        string          _title;
        int             _index;
        int             _windowMode;
        int             _rendererMode;
        Transform2D<int, int, int> transform;

        SDL_Window*     window;

        bool prepared = false;
};

#endif // RF_WINDOW_H
