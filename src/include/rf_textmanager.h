/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_TEXT_H
#define RF_TEXT_H

#include "rf_process.h"
#include "rf_structs.h"
#include "rf_engine.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string>
using namespace std;

class RF_Text : public RF_Process
{
  public:
    RF_Text():RF_Process("RF_Text"){}
    virtual ~RF_Text();

    void Configure(string text, SDL_Color color, Vector2<int> position);
};

class RF_TextManager
{
  public:
    static TTF_Font* Font;

    template<typename T = int>
    static string Write(string text, SDL_Color color, Vector2<T> position, string father = "", RF_Window* window = RF_Engine::instance->MainWindow())
    {
      assert(Font != nullptr);
      string id = RF_TaskManager::instance->newTask<RF_Text>(father, RF_Engine::getWindow(window));
      reinterpret_cast<RF_Text*>(RF_TaskManager::instance->getTask(id))->Configure(text, color, Vector2<int>((int)position.x, (int)position.y));
      return id;
    }

    static void DeleteText(string id = "");
    static void CleanWindow(int window);
};

#endif //RF_TEXT_H
