/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_DEBUGCONSOLE_H
#define RF_DEBUGCONSOLE_H

#include "rf_process.h"
#include "rf_structs.h"

#include<sys/socket.h>
#include<arpa/inet.h>
#include <map>
using namespace std;

typedef void (*RF_DebugCommandBallback)(int, const char**);
struct RF_DebugCommand
{
  string description;
  int nargs;
  RF_DebugCommandBallback callback;

  RF_DebugCommand(const char* d, int n, RF_DebugCommandBallback cback)
  {
    description = string(d);
    nargs = n;
    callback = cback;
  }
};

struct RF_Letter
{
  string lower, upper;
  RF_Letter(string l, string u)
  {
    lower = l;
    upper = u;
  }
};

void RF_DebugConsoleHelpCommand(int argc, const char* argv[]);

class RF_DebugConsoleListener : public RF_Process
{
  public:
    static RF_DebugConsoleListener* instance;
    RF_DebugConsoleListener():RF_Process("RF_DebugConsoleListener"){}
    virtual ~RF_DebugConsoleListener();

    virtual void Start();
    virtual void Update();

    static void writeLine(string text);
    inline static void addCommand(string command, RF_DebugCommand* dc)
    {
      commands[command] = dc;
    }

    void addLetter(string letter);
    void checkCommand();
    int linesSize();
    string getLine(int i);

  private:
    int keyCount = 0;
    bool hideConsole = true;
    bool keyPressed[_FOO_KEY];
    float backspaceDelay = 0.0;

    void onKeyPress();
    vector<string> lines;
    static map<string, RF_DebugCommand*> commands;

    void alterLine(string text);//, RF_DebugConsole* cmd);
    void removeLetter();
    static struct RF_Letter letters[];

    friend void RF_DebugConsoleHelpCommand(int argc, const char* argv[]);
    friend class RF_DebugConsole;
};

class RF_DebugConsole : public RF_Process
{
  public:
    static RF_DebugConsole* instance;
    RF_DebugConsole():RF_Process("RF_DebugConsole"){}
    virtual ~RF_DebugConsole();

    virtual void Start();

  private:
    int offSet = 0;
    Vector2<int> dims = {105, 47};
    vector<string> lines;

    void writeLine(string text);
    friend void RF_DebugConsoleListener::alterLine(string text);//, RF_DebugConsole* cmd);

    void changeOffset(int modifier);
    friend class RF_DebugConsoleListener;
};

#define DEBUG_BUFLEN 512

void* socketListen(void *v);
class RF_SocketDebugger
{
  public:
    RF_SocketDebugger();
    virtual ~RF_SocketDebugger(){}

    void Listen();
    static RF_SocketDebugger* instance;

    static string ip;
    static int port;

  private:
    int r = 1;
    int sock = 0, valread;
    socklen_t slen, sock_in_len, recv_len;
    struct sockaddr_in serv_addr, sock_in;
    char *msg;
    char buffer[DEBUG_BUFLEN];
    pthread_t listener;
    int signal = S_AWAKE;
};
#endif //RF_DEBUGCONSOLE_H
