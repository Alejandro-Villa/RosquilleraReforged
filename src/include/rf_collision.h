/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_COLLISION_H
#define RF_COLLISION_H

#include "rf_process.h"
#include "rf_taskmanager.h"

#include <string>
using namespace std;

#define Maximum(a, b) ((a > b) ? a : b)
#define Minimum(a, b) ((a < b) ? a : b)

inline bool rectCollision(Vector2<int>ppos1, Vector2<int> pscal1, Vector2<int>ppos2, Vector2<int> pscal2)
{
    if((ppos1.x+pscal1.x)>ppos2.x && (ppos1.y+pscal1.y)>ppos2.y && (ppos2.x+pscal2.x)>ppos1.x && (ppos2.y+pscal2.y)>ppos1.y)
    {
        return true;
    }

    return false;
}

inline SDL_Rect getIntersection(SDL_Rect boundA, SDL_Rect boundB)
{
  int x1 = Maximum(boundA.x, boundB.x);
  int y1 = Maximum(boundA.y, boundB.y);
  int x2 = Minimum(boundA.x + boundA.w, boundB.x + boundB.w);
  int y2 = Minimum(boundA.y + boundA.h, boundB.y + boundB.h);

  int width = x2 - x1;
  int height = y2 - y1;

  SDL_Rect intersect = {0,0,0,0};
  if(width > 0 && height > 0)
  {
    intersect.x = x1;
    intersect.y = y1;
    intersect.w = width;
    intersect.h = height;
  }

  return intersect;
}

inline bool checkCollision(RF_Process* entityA, RF_Process* entityB)
{
    SDL_Rect collisionRect = getIntersection(entityA->getDimensions(), entityB->getDimensions());

    if(collisionRect.w == 0 && collisionRect.h == 0)
        return false;

    SDL_Rect normalA = entityA->normalizeBounds(collisionRect);
    SDL_Rect normalB = entityB->normalizeBounds(collisionRect);

    Uint32 colorA, colorB;
    Uint8 foo, alphaA, alphaB;
    Uint8 *pA, *pB;

    for(int y = 0; y < collisionRect.h; y++)
    {
        for(int x = 0; x < collisionRect.w; x++)
        {
          pA = (Uint8*)entityA->graph->pixels + (normalA.x + y) * entityA->graph->pitch + (normalA.x + x) * entityA->graph->format->BytesPerPixel;
          SDL_GetRGBA(*(Uint32*)pA, entityA->graph->format, &foo, &foo, &foo, &alphaA);

          pB = (Uint8*)entityB->graph->pixels + (normalB.x + y) * entityB->graph->pitch + (normalB.x + x) * entityB->graph->format->BytesPerPixel;
          SDL_GetRGBA(*(Uint32*)pB, entityB->graph->format, &foo, &foo, &foo, &alphaB);

          if(alphaA > 200 && alphaB > 200)
          {
            return true;
          }
        }
    }

    return false;
}

inline RF_Process* checkCollisionByType(RF_Process* entityA, string type)
{
  for(RF_Process* entityB : RF_TaskManager::instance->getTaskByType(type))
  {
    if(checkCollision(entityA, entityB))
    {
      return entityB;
    }
  }

  return nullptr;
}

#endif //RF_COLLISION_H
