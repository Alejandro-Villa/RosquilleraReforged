/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_PROCESS_H
#define RF_PROCESS_H

#include "rf_structs.h"
#include "rf_window.h"
#include <SDL2/SDL.h>
#include <string>
#include <iostream>
#include <set>
using namespace std;

const SDL_RendererFlip FLIP_NONE = SDL_FLIP_NONE;
const SDL_RendererFlip FLIP_H = SDL_FLIP_HORIZONTAL;
const SDL_RendererFlip FLIP_V = SDL_FLIP_VERTICAL;

class RF_Process
{
  public:
    RF_Process(string _type="null", string _father="", int _window = -1)
    {
      type=_type;
      father=_father;
      graph=NULL;
      window = _window;

      transform.position = Vector2<float>(0,0);
      transform.scale = Vector2<float>(1.0,1.0);
      transform.rotation = 0;
    }

    virtual ~RF_Process(){}

    virtual void Start(){return;}
    virtual void Update(){ return;}
    virtual void FixedUpdate(){ return;}
    virtual void Draw(){ return;}
    virtual void LateDraw(){ return;}
    virtual void AfterDraw(){return;}

    /**Propiedades******/
      string id, father;
      unsigned int signal = S_AWAKE;
      string type="";
      SDL_Surface *graph;
      Transform2D<float, float, float> transform;
      int zLayer = -1;
      SDL_RendererFlip flipType = FLIP_NONE;
      //int ctype = C_SCREEN;
    /*******************/

    int window;

    bool operator<(const RF_Process* p) const
    {
      return (zLayer < p->zLayer);
    }

    SDL_Rect getDimensions()
    {
      if(graph == nullptr){return {(int)transform.position.x, (int)transform.position.y, 0, 0};}

      return {(int)transform.position.x, (int)transform.position.y, graph->w, graph->h};
    }

    virtual SDL_Rect normalizeBounds(const SDL_Rect& rect)
    {
      if(graph == nullptr){return {(int)transform.position.x, (int)transform.position.y, 0, 0};}

      SDL_Rect normalized;
      normalized.x = rect.x - (Sint16)transform.position.x + (Sint16)(graph->w >> 1);
      normalized.y = rect.y - (Sint16)transform.position.y + (Sint16)(graph->h >> 1);
      normalized.w = rect.w;
      normalized.h = rect.h;

      return normalized;
    }
};

#endif // RF_PROCESS_H
