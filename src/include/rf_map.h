/*
  LA ROSQUILLERA FRAMEWORK - REFORGED
  Copyright (C) 2017 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef RF_MAP_H
#define RF_MAP_H

#include "rf_process.h"
#include <iostream>
#include <vector>
#include <unordered_map>
#include <map>
using namespace std;

struct Node
{
    string key;
    RF_Process* data;

    Node *son;
    Node *firstChild;

    void addChild(string key, RF_Process *dat)
    {
        Node *n = new Node();
        n->key = key;
        n->data = dat;

        n->son = firstChild;
        firstChild = n;
    }
};

class RF_Map
{
    protected:
        Node *root = new Node();
        unordered_map<string, RF_Process*> taskMap;
        multimap<string, RF_Process*> typeMap;

        void Add(string key, RF_Process *dat)
        {
          vector<string> v;
          split(key,':',v);

          Node *it = root;
          Node *aux;
          for (int i = 1; i < v.size(); i++)
          {
            aux = it->firstChild;
            while (aux != nullptr && aux->key != v[i])
            {
              aux = aux->son;
            }

            if (aux != NULL && aux->key != "")
            {
              it = aux;
            }
            else
            {
              it->addChild(v[v.size()-1], dat);
              taskMap[key] = dat;
              typeMap.insert(pair<string, RF_Process*>(dat->type, dat));
              return;
            }
          }
        }

        bool isInList(string key)
        {
          return (Get(key) != nullptr);
        }

        RF_Process* Get(string key)
        {
          return taskMap[key];
        }

        void Remove(string key)
        {
            vector<string> v;
            split(key,':',v);

            Node *it = root;
            Node *aux;
            for (int i = 1; i < v.size(); i++)
            {
                aux = it->firstChild;
                while (aux != NULL && aux->key != v[i])
                {
                    aux = aux->son;
                }

                if (aux != NULL)
                {
                  if(aux->key != v[v.size()-1])
                  {
                    it = aux;
                  }
                  else
                  {
                    pair<multimap<string, RF_Process*>::iterator,multimap<string, RF_Process*>::iterator> par = typeMap.equal_range(taskMap[key]->type);
                    for(auto it = par.first; it != par.second; it++)
                    {
                      if(it->second != nullptr && it->second->id == key)
                      {
                        typeMap.erase(it);
                        break;
                      }
                    }
                    taskMap.erase(key);

                    if(it != nullptr)
                    {
                      if(aux->data->id == it->firstChild->data->id)
                      {
                        it->firstChild = aux->son;
                      }
                      else
                      {
                        auto itt = it->firstChild;
                        for(; aux->data->id != itt->son->data->id; itt = itt->son);
                        itt->son = aux->son;
                      }

                    }

                    delete aux->data;

                    aux->key = "";
                    aux->data = nullptr;
                    aux->firstChild = nullptr;
                  }
                }
            }
        }

        void RemoveAll(string key)
        {
          vector<string> v;
          split(key,':',v);

          Node *it = root;
          Node *aux;
          for (int i = 1; i < v.size(); i++)
          {
            aux = it->firstChild;
            while (aux != nullptr && aux->key != v[i])
            {
              aux = aux->son;
            }
            if (aux != nullptr)
            {
              if(i < v.size() - 1)
              {
                it = aux;
              }
              else
              {
                _Remove(aux, it);
              }
            }
          }
        }
        void _Remove(Node *aux, Node *father = nullptr)
        {
          if(aux == nullptr){return;}

          if(aux->firstChild != nullptr)
          {
            Node *i = aux->firstChild;
            Node *next;
            while(i != nullptr)
            {
              next = i->son;
              _Remove(i);
              i = next;
            }
          }

          if(aux->key != "")
          {
            pair<multimap<string, RF_Process*>::iterator,multimap<string, RF_Process*>::iterator> par = typeMap.equal_range(taskMap[aux->data->id]->type);
            for(auto it = par.first; it != par.second; it++)
            {
              if(it->second != nullptr && it->second->id == aux->data->id)
              {
                typeMap.erase(it);
                break;
              }
            }
            taskMap.erase(aux->data->id);

            if(father != nullptr)
            {
              if(aux->data->id == father->firstChild->data->id)
              {
                father->firstChild = aux->son;
              }
              else
              {
                auto it = father->firstChild;
                for(; aux->data->id != it->son->data->id; it = it->son);
                it->son = aux->son;
              }

            }

            delete aux->data;
            aux->key = "";
            aux->firstChild = nullptr;
            delete aux;
          }
        }

        void RemoveAll()
        {
          taskMap.clear();
          typeMap.clear();
        }

        void split(const string& s, char delim,vector<string>& v)
        {
            int i = 0;
            int pos = s.find(delim);
            while (pos != string::npos)
            {
                v.push_back(s.substr(i, pos-i));
                i = ++pos;
                pos = s.find(delim, pos);

                if (pos == string::npos)
                {
                    v.push_back(s.substr(i, s.length()));
                }
            }
        }
};

#endif // RF_MAP_H
