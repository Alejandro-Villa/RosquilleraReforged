#include "rf_process.h"
#include "rf_engine.h"
#include "rf_assetmanager.h"
#include "rf_layer.h"
#include "rf_collision.h"
#include "rf_soundmanager.h"

#include <SDL2/SDL.h>

class Pelota : public RF_Process
{
	public:
		Pelota():RF_Process("Pelota"){}
		virtual void Start()
		{
			transform.position = Vector2<float>(300, 300);
			graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "r");
		}
};

class Pelota2 : public RF_Process
{
	public:
		Pelota2():RF_Process("Pelota2"){}
		virtual void Start()
		{
			transform.position = Vector2<float>(100, 300);
			graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "g");
			bolita = RF_Engine::getTask(father);
		}

		virtual void Update()
		{
			float tD = RF_Engine::instance->Clock.deltaTime*50;
			if(RF_Input::key[_a])
			{
				transform.position.x -= tD;
			}
			if(RF_Input::key[_d])
			{
				transform.position.x += tD;
			}
			if(RF_Input::key[_w])
			{
				transform.position.y -= tD;
			}
			if(RF_Input::key[_s])
			{
				transform.position.y += tD;
			}

			//RF_Engine::Debug(checkCollision(this, bolita));
			if(!checkCollision(this, bolita))
			{
				//RF_SoundManager::changeMusic("common", "TestingTrack");
			}
			else
			{
				//RF_SoundManager::changeMusic("common", "CaravanPalace");
			}
		}

		RF_Process* bolita;
};

class Scene2 : public RF_Process
{
  public:
    Scene2():RF_Process("Scene2"){}
    ~Scene2()
		{
			RF_Engine::closeWindow(w);
		}

		int w;
    virtual void Start()
    {
      RF_AssetManager::LoadAssetPackage("res/common_big");
	    RF_AssetManager::LoadAssetPackage("res/common");

			w = RF_Engine::addWindow("Escena 2", 1024, 640);
			RF_Engine::MainWindow(w);
			RF_Engine::newTask<Pelota2>(RF_Engine::newTask<Pelota>(id));
    }

		string bola;
};
