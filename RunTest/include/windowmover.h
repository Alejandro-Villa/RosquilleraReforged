#include "rf_process.h"
#include "rf_window.h"

class WindowMover : public RF_Process
{
	public:
		WindowMover():RF_Process("WindowMover"){}
		virtual ~WindowMover(){}

		virtual void Start()
		{
			deriv = 0.25;//((float)(10 + rand()%20))/50.0f;

			transform.position.x = RF_Engine::getWindow(window)->x();
			transform.position.y = RF_Engine::getWindow(window)->y();

			transform.scale.x = RF_Engine::getWindow(window)->width();
			transform.scale.y = RF_Engine::getWindow(window)->height();
		}
		virtual void Draw()
		{
			if(RF_Engine::getWindow(window)!=nullptr)
			{
				RF_Engine::getWindow(window)->move({transform.position.x + (int)(cos(3.14* p * deriv *2.5)*210), transform.position.y});
			}
			p+=RF_Engine::instance->Clock.deltaTime*2;
		}
	private:
		float deriv, deriv2;
		float p = 0;
};
