#include "rf_process.h"

class TestProcess3 : public RF_Layer
{
	public:
		TestProcess3():RF_Layer("TestProcess3"){}
		virtual ~TestProcess3(){}

		virtual void Start()
		{
					RF_Layer::Start();
					SDL_SetSurfaceBlendMode(graph, SDL_BLENDMODE_ADD);
								transform.position.x = RF_Engine::getWindow(window)->width()>>1;
								transform.position.y = RF_Engine::getWindow(window)->height()>>1;
		}
		virtual void Update()
		{
			if(RF_Engine::getWindow(window)==nullptr){return;}

			deltacount += RF_Engine::instance->Clock.deltaTime;
    	if(0.025f < deltacount)
	    {
        /* Rotozoom 1 */
          metang = 3.14 * step * 0.075;

        /* Metabolas */
          b1.x=125+cos(metang*2.5)*75; b1.y=125+sin(metang*2)*75;   b1.z=15;//15
          b2.x=125+sin(metang*1.8)*75; b2.y=125+cos(metang*2.0)*75; b2.z=20;//20
          b3.x=125+sin(metang+100)*75; b3.y=125+cos(metang+50)*75;  b3.z=40;//25

				for(int i = 0; i < RF_Engine::getWindow(window)->width(); i++)
				{
					for(int j = 0; j < RF_Engine::getWindow(window)->height(); j++)
					{
						RF_Primitive::putPixel(graph, i, j, 0x000000);
					}
				}

				for(int i = 0; i < RF_Engine::getWindow(window)->width(); i++)
        {
          calci1 = pow(b1.x-i,2);
          calci2 = pow(b2.x-i,2);
          calci3 = pow(b3.x-i,2);

          for(int j = 0; j < RF_Engine::getWindow(window)->height(); j++)
          {
            calcTemp = b1.z/(pow(sqrt(calci1+pow(b1.y-j,2)),0.90)) + b2.z/(pow(sqrt(calci2+pow(b2.y-j,2)),0.90)) + b3.z/(pow(sqrt(calci3+pow(b3.y-j,2)),0.90));
						if(calcTemp > 1.70)
            {
							d1 = sqrt(pow(b1.x-(float)i, 2) + pow(b1.y-(float)j, 2));
							d2 = sqrt(pow(b2.x-(float)i, 2) + pow(b2.y-(float)j, 2));
							d3 = sqrt(pow(b3.x-(float)i, 2) + pow(b3.y-(float)j, 2));

							Uint8 r,g,b;
							Uint32 color;
								r = (Uint8)((int)d1%256);
								g = (Uint8)((int)d2%256);
								b = (Uint8)((int)d3%256);

								color = r << 16 | b << 8 | g;

							RF_Primitive::putPixel(graph, i, j, color);
						}
					}
				}

				step+=0.1;
			}
		}

		int d1, d2, d3;
		float step = 0;
		Vector3<int> b1, b2, b3;
		float metang, calci1, calci2, calci3, calcTemp;
		float deltacount = 0.025f;
};
