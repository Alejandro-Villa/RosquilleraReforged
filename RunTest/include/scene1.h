#include "rf_process.h"
#include "rf_engine.h"
#include "rf_assetmanager.h"

#include "windowmover.h"
#include "testprocess.h"
#include "testprocess2.h"
#include "testprocess3.h"

#include <string>
using namespace std;

class Scene1 : public RF_Process
{
  public:
    Scene1():RF_Process("Scene1"){}
    ~Scene1()
    {
      w = wM;
      RF_Engine::closeWindow(w++);
      RF_Engine::closeWindow(w++);
      RF_Engine::closeWindow(w++);
      RF_Engine::closeWindow(w++);
      RF_Engine::closeWindow(w++);
    }

    int w, wM;
    virtual void Start()
    {
      RF_AssetManager::LoadAssetPackage("res/phsx");
    	RF_AssetManager::LoadAssetPackage("res/common");
    	RF_AssetManager::LoadAssetPackage("res/common_big");

      string proc;
      Uint32 color[6] = {0xFF0000, 0x00FF00, 0x0000FF, 0xFFFF00, 0xFF00FF, 0x00FFFF};

      w = RF_Engine::addWindow("Pintado de píxeles", 250, 250);
      wM = w;
      RF_Engine::MainWindow(w);
        RF_Engine::getWindow(w)->move(Vector2<int>(RF_Engine::getWindow(w)->x()-129, RF_Engine::getWindow(w)->y() - 100));
        proc = RF_Engine::newTask<TestProcess>(id, w);
        for(int i = 0; i < 6; i++)
        {
          RF_Engine::getTask<TestProcess>(proc)->color[i] = color[i];
        }

    	w = RF_Engine::addWindow("Reordenamiento de capas", 250, 250, RF_Engine::MainWindow()->x() - 258,  RF_Engine::MainWindow()->y());
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "r");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "g");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "b");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "aux1");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "aux2");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("common_big", "aux3");

    	w = RF_Engine::addWindow("Metabolas RGB", 250, 250, RF_Engine::MainWindow()->x() + 258, RF_Engine::MainWindow()->y());
    		proc = RF_Engine::newTask<TestProcess3>(id, w);

    	w = RF_Engine::addWindow("Movimiento de ventana", 350, 100, RF_Engine::MainWindow()->x() - 50,  RF_Engine::MainWindow()->y() + 285);//315);
    		proc = RF_Engine::newTask<WindowMover>(id, w);
    		/*proc = RF_Engine::newTask<TestProcess>(id, w);
    		for(int i = 0; i < 6; i++)
        {
          RF_Engine::getTask<TestProcess>(proc)->color[i] = color[i];
        }*/
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("common", "r");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("common", "g");
    		proc = RF_Engine::newTask<TestProcess2>(id, w);
    		RF_Engine::getTask(proc)->graph = RF_AssetManager::Get<RF_Gfx2D>("common", "b");
    }
};
