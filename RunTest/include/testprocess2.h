#include "rf_process.h"

class TestProcess2 : public RF_Process
{
	public:
		TestProcess2():RF_Process("TestProcess2"){}
		virtual ~TestProcess2(){}

		virtual void Start()
		{
			rand();rand();rand();rand();rand();
			deriv = ((float)(10 + rand()%50))/10.0f;
			deriv2 = ((float)(10 + rand()%40))/10.0f;
		}
		virtual void Update()
		{
			if(RF_Engine::getWindow(window)==nullptr){return;}
			RF_Engine::getWindow(window)->setBackColor(125, 0, 0);

			SDL_SetSurfaceBlendMode(graph, SDL_BLENDMODE_ADD);

			transform.position.x = (RF_Engine::getWindow(window)->width()>>1)+cos(3.14*p*0.075*deriv)*90;
			transform.position.y = (RF_Engine::getWindow(window)->height()>>1)+sin(3.14*p*0.075*deriv2)*90;

			zLayer = transform.position.y;

			p+=RF_Engine::instance->Clock.deltaTime*3;
		}

		float deriv, deriv2;
		float p = 0;
};
