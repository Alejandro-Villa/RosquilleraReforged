#include "rf_process.h"
#include "rf_engine.h"
#include "rf_assetmanager.h"

#include "scene1.h"
#include "scene2.h"
class MainProcess : public RF_Process
{
  public:
    MainProcess():RF_Process("MainProcess"){}
    virtual ~MainProcess(){}

    virtual void Start()
    {
      scene = RF_Engine::newTask<Scene1>(id);
    }

    virtual void Update()
    {
      if(RF_Input::key[_esc])
      {
        RF_Engine::Status()=false;
      }

      if(RF_Input::key[_1] && !pulsado)
      {
        if(RF_TaskManager::instance->getTaskByType("Scene1").size() == 0)
        {
          RF_Engine::sendSignal(scene, S_KILL);
          scene = RF_Engine::newTask<Scene1>(id);
        }
        pulsado = true;
      }
      else if(RF_Input::key[_2] && !pulsado)
      {
        if(RF_TaskManager::instance->getTaskByType("Scene2").size() == 0)
        {
          RF_Engine::sendSignal(scene, S_KILL);
          scene = RF_Engine::newTask<Scene2>(id);
        }

        pulsado = true;
      }
      else if(RF_Input::key[_w] && !pulsado)
      {
        vector<RF_Process*> v = RF_TaskManager::instance->getTaskList();
        RF_Engine::Debug("--- PROCESOS -------------------------------");
        RF_Engine::Debug("Cantidad de procesos: " + to_string(v.size()));
        for(auto& it : v)
        {
          RF_Engine::Debug(it->id + ": " + to_string(it->signal));
        }
        RF_Engine::Debug("--------------------------------------------");
        pulsado = true;
      }
      else if(pulsado && !RF_Input::key[_w] && !RF_Input::key[_1] && !RF_Input::key[_2])
      {
        pulsado = false;
      }
    }

    bool pulsado;
    string scene = "";
};
