#include "rf_engine.h"
#include "mainprocess.h"
#include "rf_debugconsole.h"

#include <iostream>
#include <string>
using namespace std;

int main()
{
	RF_SocketDebugger::port = 12345;
	RF_Engine::Start<MainProcess>(true);
	return 0;
}
